Grafana dashboards
==================

This repository contains the dashboards deployed on the Grafana
instance used by the [Tor Project][] to monitor internal
infrastructure. It is part of a project to replace a dead Munin
deployment with Prometheus and Grafana.

Many of the dashboards here are copies of dashboards already available
in the [grafana.com dashboard database][] but some have been modified
to fix bugs or adapt to our infrastructure. They are, however,
generally designed to be used anywhere and respect the ad-hoc
standards of dashboard design. They should also work with the latest
version of Prometheus and associated exporters.

[grafana.com dashboard database]: https://grafana.com/dashboards/
[Tor Project]: https://torproject.org/

The point of this repository is to allow collaboration and welcome
improvements so that the multiplication of dashboards on grafana.com
stops somehow.

Available dashboards
--------------------

| Dashboard                    | Status     | Changes                                                                        |
|------------------------------|------------|--------------------------------------------------------------------------------|
| All load                     | new        |                                                                                |
| [Apache][]                   | unchannged |                                                                                |
| [Blackbox exporter][]        | modified   | removed bits about dns probing                                                 |
| [Bind][]                     | modified   | portability, legend, rate, other fixes                                         |
| [Cache Health][]             | new        |                                                                                |
| Disk usage                   | new        |                                                                                |
| [Grafana][]                  | unchanged  |                                                                                |
| GitLab CI overview           | new        | inspired by [this GitLab dashboard][]                                          |
| GitLab Omnibus               | modified   |                                                                                |
| [ICMP Exporter][]            | new        |                                                                                |
| Memory usage                 | new        |                                                                                |
| MinIO overview               | unchanged  | copied from a friend                                                           |
| [Node exporter][]            | unchanged  |                                                                                |
| [Node exporter comparison][] | modified   | renamed from "Node exporter server metrics", ambiguous with above              |
| [Node exporter starsliao][]  | modified   | [ascii fix][] and adaptation                                                   |
| NTP                          | ?          | possibly imported, to verify                                                   |
| Per process memory usage     | new        |                                                                                |
| Postfix Mtail                | new        | similar to [this dashboard][], but with mtail, see the [Puppet mtail module][] |
| [Postgres][]                 | modified   | buffers formulas, qps graphsize, min/max for cache                             |
| [Postgres Percona][]         | unchanged  |                                                                                |
| [Prometheus 2.0 overview][]  | unchanged  |                                                                                |
| Prometheus 2.0 stats         | modified   | modified to add disk usage and metrics scrape time, source unknown?            |
| [Smartmon textfile][]        | unchanged  |                                                                                |
| Traffic per class            | new        |                                                                                |

 [ascii fix]: https://github.com/starsliao/Prometheus/pull/87
 [Apache]: https://grafana.com/dashboards/3894/
 [Bind]: https://grafana.com/dashboards/10024/
 [Blackbox exporter]: https://grafana.com/grafana/dashboards/9719
 [Cache Health]: https://grafana.com/grafana/dashboards/11203
 [Grafana]: https://grafana.com/dashboards/3590/
 [ICMP exporter]: https://grafana.com/grafana/dashboards/12412
 [Node exporter]: https://grafana.com/dashboards/1860/
 [Node exporter server metrics]: https://grafana.com/dashboards/405/
 [Node exporter starsliao]: https://grafana.com/grafana/dashboards/11074
 [Postfix]: https://grafana.com/dashboards/10013/
 [Postgres]: https://grafana.com/dashboards/455
 [Postgres Percona]: https://github.com/percona/grafana-dashboards/blob/master/dashboards/PostgreSQL_Overview.json
 [Smartmon textfile]: https://grafana.com/dashboards/3992/
 [Prometheus 2.0 overview]: https://grafana.com/dashboards/3662/
 [this dashboard]: https://github.com/kumina/postfix_exporter/issues/21
[this GitLab dashboard]: https://gitlab.com/gitlab-com/runbooks/-/blob/c96a9189257d5f1b18ef46ca5e82d9496df1bfb6/libsonnet/stage-groups/verify-runner/job_graphs.libsonnet
[Puppet mtail module]: https://github.com/anarcat/puppet-mtail

Updating dashboards
-------------------

Dashboards in this repository can be refreshed from a running Grafana
instance by:

 1. clicking the "disk" button (`Save dashboard`) on top
 2. click the `Save JSON to file`
 3. overwrite the right file here with the saved dashboard
 4. commit and push
 5. (optional) run Puppet on the Grafana server

The naming convention is the dashboard name, in lowercase. Grafana
gives you a file name with a timestamp in it, so you will need to
rename it. Typical third step looks like:

    mv ~/Downloads/TPO\ Overview-1693950109501.json ~/src/tor/grafana-dashboards/tpo-overview.json

Pull requests and issues are welcome.

That is for "modified" or "new" dashboards above. For "unchanged"
dashboards, they can be refreshed from the grafana.com database using
the `refresh.py` script.

This repository should be available at:

<https://gitlab.torproject.org/tpo/tpa/grafana-dashboards>

But might be mirrored or forked elsewhere as well.

TODO: use [grafana-dashboard-manager][] ([Debian RFP 1003902](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1003902)) to
manage this repository. See also [tpo/tpa/team#41312](https://gitlab.torproject.org/tpo/tpa/team/-/issues/41312).

[grafana-dashboard-manager]: https://github.com/Beam-Connectivity/grafana-dashboard-manager
